import requests from '../dao/bookDao.js';

const handleError = (response) => {
  if (typeof response === 'undefined') {
    throw new Error('Something went wrong');
  } else {
    return response;
  }
};

const findAll = async () => {
  return await requests.findAll();
};

const findOne = async (id) => {
  const result = await requests.findOne(id);
  return handleError(result, id);
};

const insertBook = async (book) => {
  const newBook = {
    name: book.name,
    author: book.author,
    read: book.read
  };
  const result = await requests.insertBook(newBook);
  return handleError(result);
};

const updateBook = async (id, modifiedBook) => {
  if (modifiedBook.read && typeof modifiedBook.read !== 'boolean') {
    throw new Error('wrong values');
  }
  let book = await requests.findOne(id);
  book = handleError(book, id);
  book.name = modifiedBook.name || book.name;
  book.author = modifiedBook.author || book.author;
  // if you change read value from true to false, the above method of assigning values would fail.
  if (Object(modifiedBook).hasOwnProperty('read')) {
    book.read = modifiedBook.read;
  }
  const result = await requests.updateBook(book);
  return handleError(result);
};

const deleteBook = async(id) => {
  const result = await requests.deleteBook(id);
  return handleError(result, id);
};

export default {
  findAll,
  findOne,
  insertBook,
  updateBook,
  deleteBook
};
