import {v4} from 'uuid';
import db from '../db/db.js';
import queries from '../db/queries.js';

/* No error handling is done in dao */

const insertBook = async (book) => {
  book['user_id'] = book.user_id || null;
  const params = [v4(), ...Object.values(book)];
  const result = await db.execQuery( queries.insertBook, params );
  return result.rows[0];
};

const findOne = async (id) => {
  const result = await db.execQuery( queries.getBook, [id] );
  return result.rows[0];
};

const findAll = async () => {
  const result = await db.execQuery( queries.getAllBooks );
  return result.rows;
};

const updateBook = async (modifiedBook)=> {
  const params = Object.values(modifiedBook);
  const result = await db.execQuery( queries.updateBook, params );
  return result.rows[0];
};

const deleteBook = async (id) => {
  const result = await db.execQuery( queries.deleteBook, [id] );
  return result.rows[0];
};

export default {
  insertBook,
  findOne,
  findAll,
  updateBook,
  deleteBook
};
