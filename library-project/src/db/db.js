import pg from 'pg';
import dotenv from 'dotenv';
dotenv.config();

const environment = process.env.NODE_ENV;
const poolOptions = {
  host: process.env.PG_DB_HOST,
  user: process.env.PG_DB_USER,
  password: process.env.PG_DB_PASSWORD,
  database: process.env.PG_DB_DATABASE,
  port: process.env.PG_DB_PORT,
  ssl: environment === 'prod'
};

export const pool = environment !== 'test' ? new pg.Pool(poolOptions) : new pg.Pool();

const execQuery = async (query, params) => {
  const client = await pool.connect();
  try {
    const result = await client.query(query, params);
    return result;
  } catch (error) {
    return error;
  } finally {
    client.release();
  }
};

export default {
  execQuery,
};
