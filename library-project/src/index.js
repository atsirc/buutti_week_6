import path from 'path';
import express from 'express';
// import 'express-async-errors';
import usersRouter from './routers/userRouter.js';
import booksRouter from './routers/v1/bookRouter.js';
import {requestLogger, errorLogger} from './middleware/loggers.js';
import helmet from 'helmet';
import db from './db/db.js';
import queries from './db/queries.js';
// NODE_ENV is defined in package.json
const environment = process.env.NODE_ENV;

const addDatabases = async () => {
  const books = await db.execQuery(queries.getAllBooks);
  if (!books.rowCount) {
    const result = await Promise.all([
      await db.execQuery(queries.createBookTable),
      await db.execQuery(queries.createUserTable)
    ]);
    console.log(result);
  };
};

environment !== 'test' && addDatabases();

const errorEndpoint = (err, _req, res, _next) => {
  if (err.message.includes('no')) {
    res.status(404).send(`<h1>404 - ${err.message}</h1>`);
  } else {
    res.status(400).send(`<h1>400 - ${err.message}</h1>`);
  }
};

// using type: module in package.json makes __dirname not work
const __dirname = path.resolve();
const unknownEndpoint = (_req, res, _next) => {
  res.status(404).sendFile(__dirname + '/src/html/404.html');
};

const app = express();
app.use(express.json());
app.use(helmet());

app.use('/', express.static(__dirname + '/src/html'));

environment !== 'test' && app.use(requestLogger);
app.use('/user', usersRouter);
app.use('/api/v1/books', booksRouter);
environment !== 'test' && app.use(errorLogger);
app.use(errorEndpoint);
app.use(unknownEndpoint);

const port = process.env.APP_PORT;
environment !== 'test' && app.listen(port, () =>{
  console.log('Running on port ' +  port);
});

export default app;
