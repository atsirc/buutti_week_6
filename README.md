# Library project structure (excluding other exercices, test/, README.md, .env)

``` 
├── library-project
│   ├── Dockerfile
│   ├── package.json
│   ├── package-lock.json
│   └── src
│       ├── dao
│       │   ├── bookDao.js
│       │   └── userDao.js
│       ├── db
│       │   ├── db.js
│       │   └── queries.js
│       ├── html
│       │   ├── 404.html
│       │   └── index.html
│       ├── index.js
│       ├── middleware
│       │   └── requestLogger.js
│       ├── routers
│       │   ├── userRouter.js
│       │   └── v1
│       │       └── bookRouter.js
│       └── services
│           ├── bookService.js
│           └── userService.js
└── .gitlab-ci.yml
```
# Comments about library-project
  
1) I am using a *routers*-folder instead of *api*
2) Using own runner: works fine, but not with sast. To correct this one should fix something with docker containers, which is a bit too much for me. It's much easier to just use GitLab's runners to check if the sast tests go through, then use own runner in the future.
  
# Running mock tests don't require a working pool-connection
  
**Background**: I had by chance initiated the process.env variables applied to `pg.Pool()` in GitLab's CI/CD variables before I started trying out my pipeline. Therefore they were supplied to the test container. When pool tried to connect, it failed because there was no host to connect to.  
**Solution**: The mock tests will run fine, as long as there isn't a connection error. This can be accomplished by just not giving any options to `pg.Pool()`
```javascript
const environment = process.env.NODE_ENV;
const poolOptions = {
  host: process.env.PG_DB_HOST,
  user: process.env.PG_DB_USER,
  password: process.env.PG_DB_PASSWORD,
  database: process.env.PG_DB_DATABASE,
  port: process.env.PG_DB_PORT,
  ssl: environment === 'prod'
};

export const pool = environment !== 'test' ? new pg.Pool(poolOptions) : new pg.Pool();
```
  
# If you want a working postgres database for use in tests, here are the best instructions I could find  

I also left the commented out service creation in my code for future reference...  
  
How to set up postgres in `.gitlab-ci.yml`  
* https://docs.gitlab.com/ee/ci/services/postgres.html
* https://stackoverflow.com/questions/42966645/setting-up-postgresql-in-gitlab-ci  
  
service-example: https://docs.gitlab.com/ee/ci/services/index.html#how-services-are-linked-to-the-job  
alternative way of setting vars (haven't tried): https://gitlab.com/gitlab-examples/postgres/-/blob/master/.gitlab-ci.yml  
