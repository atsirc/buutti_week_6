import pg from 'pg';
import dotenv from 'dotenv';
dotenv.config();
const {DB_USER, DB_HOST, DB_DATABASE, DB_PASSWORD, DB_PORT} = process.env

// comment to myself: TYPOS, TYPOS, TYPOS
const pool = new pg.Pool({
  user: DB_USER,
  host: DB_HOST,
  database: DB_DATABASE,
  password: DB_PASSWORD,
  port: DB_PORT
});

const createTestTable = `CREATE TABLE IF NOT EXISTS "testitaulu" ("id" INT, "nimi" VARCHAR(255));`;

pool.query(createTestTable, async(err, res) => {
  console.log(err);
  console.log(res)
  pool.end();
})

export default pool;