import express from 'express';
import pool from './db.js';

const app = express();

const port = 3000;

app.listen(port, () => console.log('listening to port :' + port));